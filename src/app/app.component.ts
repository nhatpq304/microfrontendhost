import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router) {
  }

  open1() {
    this.router.navigate(["bond"]);
  }

  open2() {
    this.router.navigate(["insurance"]);
  }

  open3() {
    this.router.navigate(["react"]);
  }
}
