import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';


const ROUTES: Routes = [
  {
    path: "insurance",
    loadChildren: () => import("./insurance/insurance.module").then(m => m.InsuranceModule),
  },
  {
    path: "bond",
    loadChildren: () => import("./bond/bond.module").then(m => m.BondModule),
  },
  {
    path: "react",
    loadChildren: () => import("src/app/react/react.module").then(m => m.ReactModule),
  }
];

  @NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
