import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BondComponent } from '../bond/bond.component';
import { MainComponent } from '../bond/main/main.component';
import { UtilsService } from '../utils/utils.service';

@NgModule({
  declarations: [
    BondComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        component: BondComponent,
        children: [
          {
            path: "",
            component: MainComponent,
          },
          {
            path: "first",
            component: MainComponent,
            children: [
              {
                path: "detail/:id",
                component: MainComponent,
              },
            ]
          },
          {
            path: "second",
            component: MainComponent,
          }
        ]
      },
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BondModule {
  constructor(private utils: UtilsService) {
    utils.loadScript('../web-components/colonial-bond.js');
  }
}
