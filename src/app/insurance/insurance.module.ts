import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InsuranceComponent } from '../insurance/insurance.component';
import { UtilsService } from '../utils/utils.service';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    InsuranceComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        component: InsuranceComponent,
        children: [
          {
            path: "",
            component: MainComponent,
          },
          {
            path: "first",
            component: MainComponent,
          },
          {
            path: "second",
            component: MainComponent,
          }
        ]
      },
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InsuranceModule {
  constructor(private utils: UtilsService) {
    utils.loadScript('../web-components/colonial-insurance.js');
  }
}
